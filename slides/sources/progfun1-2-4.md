% Scala Syntax Summary
%
%

Language Elements Seen So Far:
==============================

We have seen language elements to express types, expressions
and definitions.

Below, we give their context-free syntax in Extended Backus-Naur form (EBNF),
where

$\gap$   `|` denotes an alternative, \par

$\gap$   `[...]` an option (0 or 1), \par

$\gap$   `{...}` a repetition (0 or more).

Types
=====

\begin{lstlisting}
Type         = SimpleType | FunctionType
FunctionType = SimpleType `=>' Type
             | `(' [Types] `)' `=>' Type
SimpleType   = Ident
Types        = Type {`,' Type}
\end{lstlisting}

A \red{type} can be:

 - A \red{numeric type}: `Int`, `Double` (and `Byte`, `Short`, `Char`, `Long`, `Float`),
 - The `Boolean` type with the values `true` and `false`,
 - The `String` type,
 - A \red{function type}, like `Int => Int`, `(Int, Int) => Int`.

Later we will see more forms of types.

Expressions
===========

\begin{lstlisting}
Expr         = InfixExpr | FunctionExpr
             | if Expr then Expr else Expr
InfixExpr    = PrefixExpr | InfixExpr Operator InfixExpr
Operator     = ident
PrefixExpr   = [`+' | `-' | `!' | `~' ] SimpleExpr
SimpleExpr   = ident | literal | SimpleExpr `.' ident
             | Block
FunctionExpr = Bindings `=>` Expr
Bindings     = ident
             | `(' [Binding {`,' Binding}] `)'
Binding      = ident [`:' Type]
Block        = `{' {Def `;'} Expr `}'
             | <indent> {Def `;'} Expr <outdent>
\end{lstlisting}

Expressions (2)
===============

An \red{expression} can be:

- An \red{identifier} such as `x`, `isGoodEnough`,
- A \red{literal}, like `0`, `1.0`, `"abc"`,
- A \red{function application}, like `sqrt(x)`,
- An \red{operator application}, like `-x`, `y + x`,
- A \red{selection}, like `math.abs`,
- A \red{conditional expression}, like `if x < 0 then -x else x`,
- A \red{block}, like `{ val x = math.abs(y) ; x * 2 }`
- An \red{anonymous function}, like `x => x + 1`.

Definitions
===========

\begin{lstlisting}
Def          = FunDef  |  ValDef
FunDef       = def ident {`(' [Parameters] `)'}
               [`:' Type] `=' Expr
ValDef       = val ident [`:' Type] `=' Expr
Parameter    = ident `:' [ `=>' ] Type
Parameters   = Parameter {`,' Parameter}
\end{lstlisting}

A \red{definition} can be:

- A \red{function definition}, like `def square(x: Int) = x * x`
- A \red{value definition}, like `val y = square(2)`

A \red{parameter} can be:

- A \red{call-by-value parameter}, like `(x: Int)`,
- A \red{call-by-name parameter}, like `(y: => Double)`.

